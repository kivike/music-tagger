# -*- coding: utf-8 -*-
#
# kivike 2018
# Artist / album artist formatter

from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
import re
import sys
import fnmatch
import os
import datetime
import time

# EasyID3
KEY_ARTIST = 'artist'
KEY_ALBUM_ARTIST = 'albumartist'

# ID3
#FRAME_ARTIST = 'TPE1'
#FRAME_ALBUM_ARTIST = 'TPE2'

artist_map = {}
skipped_artists = []

if len(sys.argv) < 2:
    exit('Missing parameters')

ROOT_PATH=sys.argv[1]

if not os.path.exists(os.path.dirname(ROOT_PATH)):
    exit('Invalid path argument')

def main():
    print "finding files"
    files = find_mp3_files(ROOT_PATH)
    print "%d mp3s found" % len(files)

    start_time = time.time()
    for file in files:
        try:
            tags = MP3(file, ID3=EasyID3)

            if format_korean_artist_fields(tags):
                print "saving tags for " + file.encode('utf-8')
                tags.save()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            print "%s Unexpected error: %s %s" \
                % (file.encode('utf-8'), e.__class__.__name__, e)

            exc_type, exc_obj, tb = sys.exc_info()

    print "Total time: %.03fs" % (time.time() - start_time)


def find_mp3_files(path):
    files = []

    for root, dirnames, filenames in os.walk(path.decode('utf-8')):
        filenames = fnmatch.filter(filenames, '*.mp3')
        files.extend(map(lambda l: os.path.join(root, l), filenames))

    return files

def is_korean(string):
    for i in unicode(string, 'utf-8'):
        val = ord(i)
        if (val >= 4352 and val <= 4607) or (val >= 44032 and val <= 55203):
            return True
    return False

def format_korean_artist_fields(track):
    tags_updated = False

    try:
        artist = track[KEY_ARTIST][0].encode('utf-8')

        try:
            album_artist = track[KEY_ALBUM_ARTIST][0].encode('utf-8')
        except KeyError as e:
            album_artist = artist
    except Exception as e:
        print "Failed reading tags " + e
        return tags_updated

    formatted_artist = format_korean_artist(artist)

    if formatted_artist != artist:
        track[KEY_ARTIST] = formatted_artist.decode('utf-8')
        tags_updated = True

    if artist == album_artist:
        formatted_album_artist = formatted_artist
    else:
        formatted_album_artist = format_korean_artist(album_artist)

    if formatted_album_artist != album_artist:
        track[KEY_ALBUM_ARTIST] = formatted_artist.decode('utf-8')
        tags_updated = True

    return tags_updated

def format_korean_artist(artist):
    if artist in skipped_artists:
        return artist
    if artist_map.get(artist):
        return artist_map.get(artist)

    orig_artist = artist

    pattern = '^(.+) [([](.+)[)\]]$'

    matches = re.search(pattern, artist);

    if matches:
        primary_name = matches.group(1)
        secondary_name = matches.group(2)

        if is_korean(primary_name) and not is_korean(secondary_name):
            artist_candidate = secondary_name + " (" + primary_name + ")"

            print '"%s" => "%s"' % (artist, artist_candidate)

            try:
                confirm = raw_input('confirm? ')
            except:
                confirm = 'n'

            if confirm == 'y':
                artist_map[orig_artist] = artist_candidate
                artist = artist_candidate
            else:
                skipped_artists.append(artist)

    return artist

if __name__ == '__main__':
    main()